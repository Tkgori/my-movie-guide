package com.kgori.mymovieguide.fragments;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.ybq.android.spinkit.SpinKitView;
import com.kgori.mymovieguide.R;
import com.kgori.mymovieguide.activities.ViewMovieInfo;
import com.kgori.mymovieguide.models.Movie;
import com.kgori.mymovieguide.responses.MovieResponse;
import com.kgori.mymovieguide.utils.Endpoints;
import com.kgori.mymovieguide.utils.MovieAdapter;
import com.kgori.mymovieguide.utils.RetrofitClientModel;
import com.kgori.mymovieguide.utils.constants;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * A simple {@link Fragment} subclass.
 */
public class NowPlaying extends Fragment implements MovieAdapter.OnMovieClickedListener {

    private RecyclerView recyclerView;
    private SpinKitView loading;
    private List<Movie> nowPlayingMovies = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_now_playing, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        recyclerView = (RecyclerView) view.findViewById(R.id.now_playing_recyclerview);
        loading = (SpinKitView) view.findViewById(R.id.now_playing_spin_kit);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        super.onViewCreated(view, savedInstanceState);
        loading.setVisibility(view.VISIBLE);
        getMovies();

    }

    private void getMovies() {

        Retrofit retrofit = RetrofitClientModel.getRetrofitClent();
        Endpoints endpoints = retrofit.create(Endpoints.class);
        Call call = endpoints.currentlyShowingMovies(constants.api_key,constants.language);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if(response!=null){
                    MovieResponse movieResponse = (MovieResponse) response.body();
                    nowPlayingMovies = movieResponse.getMovies();
                    MovieAdapter movieAdapter = new MovieAdapter(getContext(), nowPlayingMovies, NowPlaying.this);
                    recyclerView.setAdapter(movieAdapter);
                    loading.setVisibility(View.INVISIBLE);
                }
            }
            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });
    }

    @Override
    public void onMovieClick(int position) {
        String movie_id = String.valueOf(nowPlayingMovies.get(position).getId());
        double popularity = nowPlayingMovies.get(position).getPopularity();

        Bundle bundle = new Bundle();
        Intent openMovieInfo = new Intent(getActivity(), ViewMovieInfo.class);
        bundle.putString("movie_id", movie_id);
        bundle.putDouble("pop", popularity);
        openMovieInfo.putExtras(bundle);
        startActivity(openMovieInfo);
    }
}
