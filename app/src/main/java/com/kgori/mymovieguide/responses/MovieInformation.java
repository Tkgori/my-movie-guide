package com.kgori.mymovieguide.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kgori.mymovieguide.models.Genre;
import com.kgori.mymovieguide.models.SpokenLanguage;

import java.util.List;

public class MovieInformation {

    @SerializedName("original_title")
    @Expose
    String title;

    @SerializedName("vote_average")
    @Expose
    double vote_average;

    public double getVote_average() {
        return vote_average;
    }

    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    @SerializedName("overview")
    @Expose
    String overview;

    @SerializedName("release_date")
    @Expose
    String releaseDate;

    @SerializedName("genres")
    @Expose
    List<Genre> genres;

    @SerializedName("poster_path")
    @Expose
    String poster_path;

    @SerializedName("spoken_languages")
    @Expose
    List<SpokenLanguage>spokenLanguages;

    public List<SpokenLanguage> getSpokenLanguages() {
        return spokenLanguages;
    }

    public void setSpokenLanguages(List<SpokenLanguage> spokenLanguages) {
        this.spokenLanguages = spokenLanguages;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

}
