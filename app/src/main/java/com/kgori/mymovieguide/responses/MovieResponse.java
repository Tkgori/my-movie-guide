package com.kgori.mymovieguide.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.kgori.mymovieguide.models.Movie;

import java.util.List;

public class MovieResponse {

    @SerializedName("results")
    @Expose
    List<Movie> movies;

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
