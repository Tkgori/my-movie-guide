package com.kgori.mymovieguide.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.kgori.mymovieguide.R;

public class MainActivity extends AppCompatActivity {

    TextView loading;
    Handler handler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        YoYo.with(Techniques.FadeIn).duration(5000).repeat(0).playOn(findViewById(R.id.app_name));
        loading = (TextView) findViewById(R.id.loading_text);

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
               Intent intent = new Intent(MainActivity.this, MovieContainer.class);
               startActivity(intent);
               finish();
            }
        },5000);

    }
}
