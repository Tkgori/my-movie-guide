package com.kgori.mymovieguide.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.kgori.mymovieguide.R;
import com.kgori.mymovieguide.fragments.NowPlaying;
import com.kgori.mymovieguide.fragments.PopularMovies;
import com.kgori.mymovieguide.fragments.TopRatedMovies;
import com.kgori.mymovieguide.fragments.UpcomingMovies;

public class MovieContainer extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener{
    BottomNavigationView bottomNav;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_container);
        bottomNav = (BottomNavigationView)findViewById(R.id.bottom_nav);
        bottomNav.setOnNavigationItemSelectedListener(this);
        loadFragment(new NowPlaying());
    }
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment;

        switch (menuItem.getItemId()){

            case R.id.now_playing:
                fragment = new NowPlaying();
                loadFragment(fragment);
                return true;

            case R.id.popular:
                fragment = new PopularMovies();
                loadFragment(fragment);
                return true;

            case R.id.upcoming:
                fragment = new UpcomingMovies();
                loadFragment(fragment);
                return true;

            case R.id.top_rated:
                fragment = new TopRatedMovies();
                loadFragment(fragment);
                return true;
        }
        return false;
    }

    private void loadFragment(Fragment fragment){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
