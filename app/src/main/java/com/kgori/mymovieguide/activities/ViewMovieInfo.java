package com.kgori.mymovieguide.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.lzyzsd.circleprogress.ArcProgress;
import com.kgori.mymovieguide.R;
import com.kgori.mymovieguide.models.Genre;
import com.kgori.mymovieguide.models.Movie;
import com.kgori.mymovieguide.models.SpokenLanguage;
import com.kgori.mymovieguide.responses.MovieInformation;
import com.kgori.mymovieguide.utils.Endpoints;
import com.kgori.mymovieguide.utils.RetrofitClientModel;
import com.kgori.mymovieguide.utils.constants;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ViewMovieInfo extends AppCompatActivity {

    ArcProgress average, popularity;
    TextView moviename, releasedate, genres, langs, info;
    ImageView moviepic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_movie_info);
        Bundle bundle = getIntent().getExtras();
        String movie_id = bundle.getString("movie_id");
        double pop = bundle.getDouble("pop");

        moviename = (TextView) findViewById(R.id.movie_info_name);
        releasedate = (TextView) findViewById(R.id.release_date);
        genres = (TextView) findViewById(R.id.genres);
        langs = (TextView) findViewById(R.id.lang);
        info = (TextView) findViewById(R.id.movie_info);
        average = (ArcProgress) findViewById(R.id.avg);
        popularity = (ArcProgress) findViewById(R.id.pop);
        moviepic = (ImageView) findViewById(R.id.movie_pic);
        getMovieInformation(movie_id, pop);

    }

    private void getMovieInformation(final String movie_id, final double pop) {

        Retrofit retrofit = RetrofitClientModel.getRetrofitClent();
        Endpoints endpoints = retrofit.create(Endpoints.class);
        Call call = endpoints.movieInformation(movie_id, constants.api_key);
        call.enqueue(new Callback() {
            @Override
            public void onResponse(Call call, Response response) {
                if(response.body() != null){
                    MovieInformation movieInformation = (MovieInformation) response.body();

                    Picasso.with(ViewMovieInfo.this).load(constants.image_url+ movieInformation.getPoster_path()).into(moviepic);
                    moviename.setText(movieInformation.getTitle());
                    releasedate.setText("Released On: " + movieInformation.getReleaseDate());
                    for(Genre genre : movieInformation.getGenres()){
                        genres.append(genre.getName());
                        genres.append("\n");
                    }
                    langs.append("Languages");
                    langs.append("\n");
                    for(SpokenLanguage language : movieInformation.getSpokenLanguages()){
                        langs.append(language.getName());
                        langs.append("\n");
                    }
                    average.setProgress((int) movieInformation.getVote_average());
                    popularity.setProgress((int)pop);
                    info.setText(movieInformation.getOverview());
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {

            }
        });


    }
}
