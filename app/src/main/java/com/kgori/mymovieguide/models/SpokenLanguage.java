package com.kgori.mymovieguide.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SpokenLanguage {
    @SerializedName("iso_3166_1")
    @Expose
    String iso;

    @SerializedName("name")
    @Expose
    String name;

    public String getIso() {
        return iso;
    }

    public void setIso(String iso) {
        this.iso = iso;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
