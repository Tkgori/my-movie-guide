package com.kgori.mymovieguide.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kgori.mymovieguide.R;
import com.kgori.mymovieguide.models.Movie;
import com.squareup.picasso.Picasso;
import java.util.List;

public class MovieAdapter  extends RecyclerView.Adapter<MovieAdapter.ViewHolder>  {

    private List<Movie> movies;
    private Context context;
    private LayoutInflater inflater;
    private OnMovieClickedListener movieClickedListener;


    public MovieAdapter(Context context, List<Movie> movies, OnMovieClickedListener movieClickedListener) {
        this.movieClickedListener = movieClickedListener;
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.movies = movies;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.movie_layout, parent, false);
        return new ViewHolder(view, movieClickedListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Picasso.with(context).load(constants.image_url + movies.get(position).getPoster_path()).into(holder.movie_image);
        holder.movie_name.setText(movies.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        OnMovieClickedListener onMovieClickedListener;
        ImageView movie_image;
        TextView movie_name;


        public ViewHolder(@NonNull View itemView, OnMovieClickedListener onMovieClickedListener) {
            super(itemView);
            movie_image = (ImageView) itemView.findViewById(R.id.movie_image);
            movie_name = (TextView) itemView.findViewById(R.id.movie_name);
            this.onMovieClickedListener = onMovieClickedListener;
            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View view) {
            onMovieClickedListener.onMovieClick(getAdapterPosition());
        }
    }
    public interface OnMovieClickedListener{
        void onMovieClick(int position);
    }

}
