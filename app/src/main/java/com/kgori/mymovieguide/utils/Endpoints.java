package com.kgori.mymovieguide.utils;

import com.kgori.mymovieguide.responses.MovieInformation;
import com.kgori.mymovieguide.responses.MovieResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Endpoints {

    //gets movies that are now playing
    @POST("/3/movie/now_playing")
    Call<MovieResponse> currentlyShowingMovies(@Query("api_key") String apikey, @Query("language") String language);

    @POST("/3/movie/popular")
    Call<MovieResponse> currentlyPopularMovies(@Query("api_key") String apikey, @Query("language") String language);

    @POST("/3/movie/upcoming")
    Call<MovieResponse> upcomingMovies(@Query("api_key") String apikey, @Query("language") String language);

    @POST("/3/movie/top_rated")
    Call<MovieResponse> topRatedMovies(@Query("api_key") String apikey, @Query("language") String language);

    @GET("/3/movie/{movie_id}")
    Call<MovieInformation> movieInformation(@Path("movie_id") String movie_id, @Query("api_key") String apikey);



}
