package com.kgori.mymovieguide.utils;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;




public class RetrofitClientModel {

    public static Retrofit retrofit;

    public static Retrofit getRetrofitClent(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(constants.base_url)
                    .addConverterFactory(GsonConverterFactory.create()).build();
        }
        return retrofit;
    }

}
